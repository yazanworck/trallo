<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Listtascs;
use App\Models\Tascs;

class HomeController extends Controller
{
    public function index()
    {


        // $newTasc=Listtascs::find(1)->tasc;
        // dd($newTasc);


        // $tasc = Listtascs::find(1)->tasc;
        // dd($tasc);

        // foreach ($tasc as $item) {
        //     //
        // }
        $ListtascsGet = Listtascs::get();
        $tascsGet = Tascs::get();
        // foreach ($Listtascs as $lists) {
        //     dd($lists);
        // }
        // dd($Tascs, $Listtascs);
        $tascs  = $tascsGet->find(2);
        $ArryWithTascsAndList = [];
        foreach ($tascsGet as $el) {
            // dd($el->find($el->id));
            array_push($ArryWithTascsAndList, (object)[
                "tasck" => $el,
                "listtasc" => $el->listtascs
            ]);
        }
        // dd($ArryWithTascsAndList[0]->tasck->title,$ArryWithTascsAndList[0]->listtasc[0]->title);
        // dd($tascs, $tascs->listtascs);
        // $Listtascs->TascsFun()->where('listtacs_id', 1)->get();


        return view('home.index', compact('ArryWithTascsAndList'));
    }
    public function submit(Request $req)
    {

        $listTascs = new Tascs;
        // dd();

        $listTascs->title = $req->input('name');
        // $listTascs->tascs_id = 1;


        $listTascs->save();


        // dd($req->input('name'));
        return redirect()->route('home.index');
    }
    public function formComment(Request $req)
    {

        $listTascs = new Listtascs();
        // dd();

        $listTascs->title = $req->input('name');
        $listTascs->tascs_id = $req->input('tascs_id');


        $listTascs->save();


        // dd($req->input('name'));
        return redirect()->route('home.index');
    }
    public function updata($id)
    {
        $tascsGet = Tascs::get();
        $tascs  = $tascsGet->find($id);
        $ArryWithTascsAndList = [];

        // dd($el->find($el->id));
        array_push($ArryWithTascsAndList, (object)[
            "tasck" => $tascs,
            "listtasc" => $tascs->listtascs
        ]);


        // dd($ArryWithTascsAndList);


        return view('home.updata', compact('ArryWithTascsAndList'));
    }
    public function updating($id, Request $req)
    {

        $tascs = Tascs::find($id);
        // dd($tascs->title);

        $tascs->title = $req->input('tasc'.$tascs->id);
        // $listTascs->tascs_id = 1;
        foreach($tascs->listtascs as $coment){
            $tascscom = Listtascs::find($coment->id);
            $tascscom->title = $req->input('com' . $coment->id);
            // dd( $req->input('com' . $coment->id));
            $tascscom ->save();
        }



        $tascs->save();


        // dd($req->input('name'));
        return redirect()->route('home.index');
    }
}
