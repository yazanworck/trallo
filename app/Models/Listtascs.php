<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Listtascs extends Model
{
    use HasFactory;

    // protected $primaryKey = 'listtascs_id';
    public function tascs()
    {
        return $this->belongsTo(Tascs::class);
    }

}

