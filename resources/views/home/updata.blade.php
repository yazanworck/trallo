@extends('layouts.app-master')

@section('content')
    @auth
        <!-- Board info bar -->
        <section class="board-info-bar">

            <div class="board-controls">

                <button class="board-title btn">
                    <h2>Web Development</h2>
                </button>

                <button class="star-btn btn" aria-label="Star Board">
                    <i class="far fa-star" aria-hidden="true"></i>
                </button>

                <button class="personal-btn btn">Personal</button>

                <button class="private-btn btn"><i class="fas fa-briefcase private-btn-icon"
                        aria-hidden="true"></i>Private</button>

            </div>

            <button class="menu-btn btn"><i class="fas fa-ellipsis-h menu-btn-icon" aria-hidden="true"></i>Show Menu</button>

        </section>
        <!-- End of board info bar -->

        <!-- Lists container -->
        <section class="lists-container">
            @foreach ($ArryWithTascsAndList as $element)
            <form action="{{ route('home.updating',$element->tasck->id) }}" method="post">
                @csrf
                <div class="list">



                    <h3 class="list-title "><input type="text" name="tasc{{$element->tasck->id}}" id="tasc{{$element->tasck->id}}" value="{{$element->tasck->title}}"></h3>



                    <ul class="list-items">
                        @foreach ($element->listtasc as $item )

                        <li ><input type="text" name="com{{$item->id}}" id="com{{$item->id}}" value="{{$item->title}}"></li>


                        @endforeach
                    </ul>

                    <button class="add-card-btn btn" type="submit">
                        <!-- Trigger/Open The Modal -->
                        updata


                    </button>


                </div>
            </form>
            @endforeach
            

            {{-- <button class="add-list-btn btn myBtn"> --}}
            <button class="add-list-btn btn myBtn">
                <!-- Trigger/Open The Modal -->
                Add a list


            </button>
            <!-- The Modal -->
            <div class="container mt-3 modal myModal">
                <span class="close">&times;</span>

                <form action="{{ route('home.form') }}" method="post">
                    @csrf
                    <div class="form-gruop mb-3 mt-3">
                        <label for="name"> Task name</label>
                        <input type="text" name="name" id="name" class="form-control">
                        {{-- <input type="text" name="tascs_id" id="tascs_id" class="form-control"> --}}

                    </div>
                    <button type="submit" class="btn btn-success"> ok </button>
                </form>
            </div>


           

        </section>
    @endauth

    @guest

        <div class="col-md-center">
            <h1>Homepage</h1>
            <p class="lead">Your viewing the home page. Please login to view the restricted data.</p>
        </div>
    @endguest
@endsection
