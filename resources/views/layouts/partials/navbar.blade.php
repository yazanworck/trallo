<header class="masthead">

    <div class="boards-menu">

        <button class="boards-btn btn"><i class="fab fa-trello boards-btn-icon"
                style="pointer-events: none;"></i>Boards</button>

        {{-- <div class="board-search">
        <input type="search" class="board-search-input" aria-label="Board Search">
        <i class="fas fa-search search-icon" aria-hidden="true"></i>
      </div> --}}

    </div>

    <div class="logo" style="">

        <h1><i class="fab fa-trello logo-icon" aria-hidden="true"></i>
            @auth

                {{ auth()->user()->username }}</h1>
        @endauth

    </div>

    <div class="user-settings">

        <button class="user-settings-btn btn myBtn" aria-label="Create">
            <i class="fas fa-plus " aria-hidden="true"> tasc</i>
        </button>

        <button class="user-settings-btn btn myBtn1" aria-label="Information">
            <i class="fas fa-plus " aria-hidden="true"> com</i>
        </button>

        {{-- <button class="user-settings-btn btn" aria-label="Notifications">
        <i class="fas fa-bell" aria-hidden="true"></i>
      </button> --}}

        @auth
            {{ auth()->user()->name }}
            <button href="{{ route('logout.perform') }}" class="user-settings-btn btn" aria-label="Notifications"
                style="width: 55px">

                <a href="{{ route('logout.perform') }}" class="btn btn-outline-light">Logout</a>

            </button>
        @endauth

        @guest
            <button href="{{ route('login.perform') }}" class="user-settings-btn btn" aria-label="Notifications"
                style="width: 55px">

                <a href="{{ route('login.perform') }}" class="btn btn-outline-light">Login</a>

            </button>
            <button class="user-settings-btn btn" aria-label="Notifications" style="width: 60px">
                <a href="{{ route('register.perform') }}" class="btn btn-warning">Sign-up</a>
            </button>
        @endguest

    </div>

</header>
